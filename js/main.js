(function ($) {
  $(document).ready(function () {

    var isMobile = navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/);
    var verticalThumbsSetUp = false;

    // Cache
    var $thumbs = $('#thumbs');
    var $display = $('#display');
    var $thumbs_li = $thumbs.find('> .carousel-inner > .item > ul > li');
    var $thumbsCarouselInner = $thumbs.find('.carousel-inner');

    // Add some extra class when we are on mobile devices.
    isMobile && $display.addClass('mobile') && $thumbs.addClass('mobile');

    // Reset interval. We don't need it.
    $display.carousel({interval: false});
    !isMobile && $thumbs.carousel({interval: false});

    !isMobile && $('.thumbnail > a.action').click(function (e) {
      e.preventDefault();
      var $item = $(this).parent();

      $thumbs_li.removeClass('selected');
      $item.addClass('selected');
      $display.carousel(parseInt($item.attr('data-slide-display-to'), 10));
    });

    $display.on('slide.bs.carousel', function (e) {
      var currentItem = $.fn.carousel.Constructor.prototype.getItemIndex($(e.relatedTarget));
      var $thumb = $thumbs_li.filter('[data-slide-display-to="' + currentItem + '"]');

      $thumbs_li.removeClass('selected');
      $thumb.addClass('selected');
      $thumbs.carousel(parseInt($(e.relatedTarget).attr('data-slide-thumbs-belongs-to'), 10));
    });

    // @todo hacer unbind cuando ya tengamos lo que necesitamos.
    !isMobile && $thumbs.on('slide.bs.carousel', function (e) {

      if ($thumbs.hasClass('vertical')) {
        // Esto tiene un solo cometido. Setear explicitamente el alto de los thumbs verticales.
        // Sin este valor te vas olvidando de que los slides funcionen como dios manda.
        if (!verticalThumbsSetUp) {
          $thumbsCarouselInner.css('height', $thumbsCarouselInner.height());
          verticalThumbsSetUp = true;
        }
      }

    });

    $display.swiperight(function () {
      $(this).carousel('prev');
    });

    $display.swipeleft(function () {
      $(this).carousel('next');
    });

    // Prevent image draggind. it's annoying.
    $('.carousel-inner > .item img').on('dragstart', function (event) {
      event.preventDefault();
    });
  });

})(jQuery);