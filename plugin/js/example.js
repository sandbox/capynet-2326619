$(document).ready(function () {

  $('.resize').on('click', function (e) {
    e.preventDefault();
    var $this = $(this);
    $('.example-container').animate({width: $this.data('size')});
  });

});