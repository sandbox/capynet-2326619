<div class="slide carousel" id="thumbs">
  <div class="carousel-inner">

    <?php foreach ($groups as $gid => $group): ?>

      <div class="item <?php if (isset($group['is-first'])): print "active"; endif; ?>">

        <ul class="thumbnails">

          <?php foreach ($group['items'] as $delta => $item): ?>
            <li class="thumbnail<?php if ($delta == 0): print " selected"; endif; ?>" data-slide-display-to="<?php print $delta; ?>">
              <a href="#" class="action">Select</a>
              <?php print render($item); ?>
            </li>
          <?php endforeach; ?>

        </ul>
      </div>

    <?php endforeach; ?>

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#thumbs" data-slide="prev"></a>
  <a class="right carousel-control" href="#thumbs" data-slide="next"></a>

</div>