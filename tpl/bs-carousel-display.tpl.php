<div id="display" class="carousel slide">

  <ol class="carousel-indicators">
    <?php foreach ($items as $delta => $item): ?>
      <li data-target="#display" class="<?php if ($item['is-first']): print "active"; endif; ?>" data-slide-to="<?php print $delta; ?>"></li>
    <?php endforeach; ?>
  </ol>

  <span class="zoom" title="Expand image"></span>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <?php foreach ($items as $delta => $item): ?>
      <div class="item <?php if ($item['is-first']): print "active"; endif; ?>" data-slide-thumbs-belongs-to="<?php print $item['current-group']; ?>">
        <?php print render($item['img']); ?>
      </div>
    <?php endforeach; ?>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#display" data-slide="prev"></a>
  <a class="right carousel-control" href="#display" data-slide="next"></a>
</div>